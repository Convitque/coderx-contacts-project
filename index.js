/**
 * Sử dụng kiến thức đã học, tạo ra một ứng dụng danh bạ điện thoại, có các chức năng:
 * - Nhập dữ liệu contact (name, phone number)
 * - Sửa dữ liệu contact
 * - Xoá contact
 * - Tìm kiếm contact: có thể nhập vào tên (không dấu hoặc có dấu, chữ hoa hoặc chữ thường vẫn cho ra kết quả) hoặc 1 phần số điện thoại
 */

const readlineSync = require('readline-sync')
const fs = require('fs');

let contacts ;

function loadData() {
    let data = fs.readFileSync('./data.json');
    contacts = JSON.parse(data);
}

function showContacts() {
    for(let contact of contacts) {
        console.log(contact.name);
    }
}

function showMenu() {
    loadData();
    console.log("=======================================")
    console.log("1. Show contact");
    console.log("2. Create contact");
    console.log("3. Find contact");
    console.log("4. Edit contact");
    console.log("5. Delete contact");

    let option = readlineSync.question('> ');
    switch(option) {
        case "1":
            showContacts();
            showMenu();
            break;
        case "2":
            createContact();
            showMenu();
            break;
        case "3":
            findContacts();
            showMenu();
            break;
        case "4":
            deleteContact();
            showMenu();

    }
}


function createContact() {
    let name = readlineSync.question('Name: ');
    let phoneNumber = readlineSync.question('Phone number: ');
    let contact = {
        name: name,
        phoneNumber: phoneNumber
    }
    contacts.push(contact);
    save();
}

function findContacts() {
    let input = readlineSync.question('Search > ').toLowerCase();
    console.log(input);
    for(let contact of contacts) {
        if(contact.name.search(input) >=0 || contact.phoneNumber.search(input) >=0) console.log(contact.name, contact.phoneNumber);
    }
}

function deleteContact() {
    let input = readlineSync.question('Search > ').toLowerCase();
    for(let i =0; i<contacts.length;i++) {
        if(contacts[i].name.search(input) >= 0 || contacts[i].phoneNumber.search(input) >= 0) {
            console.log("delete ", contacts[i]);
            contacts.slice(i,i);
        }
    }
}

function save() {
    let data = JSON.stringify(contacts);
    fs.writeFileSync('./data.json',data,{encoding:"utf8"});
}

function main() {
    showMenu();
}

main();